  <div data-collapse="medium" data-animation="default" data-duration="400" id="w-node-ae02d7d28545-d7d28545" class="navbar w-nav">
    <div class="nav-container"><a href="dendritic-spine-analysis.html" class="brand w-nav-brand"><img src="images/logofull_EM-dark.png" srcset="images/logofull_EM-dark-p-500.png 500w, images/logofull_EM-dark-p-800.png 800w, images/logofull_EM-dark-p-1080.png 1080w, images/logofull_EM-dark-p-1600.png 1600w, images/logofull_EM-dark-p-2000.png 2000w, images/logofull_EM-dark.png 2422w" sizes="(max-width: 991px) 420px, 360px" alt="" class="logo-image"></a>
      <nav role="navigation" class="nav-menu w-nav-menu">
        <a href="index.html" class="nav-link w-inline-block">
          <div class="div-block"><img src="images/Tilda_Icons_11mu_house.svg" alt="" class="icon-btn">
            <div>Home</div>
          </div>
        </a>
        <a href="#" class="nav-link w-inline-block">
          <div class="div-block"><img src="images/icons_1ed_group.svg" alt="" class="icon-btn">
            <div>Companies</div>
          </div>
        </a>
        <a href="services.html" class="nav-link w-inline-block">
          <div class="div-block"><img src="images/Tilda_Icons_37_Finance-technologies_big-data.svg" alt="" class="icon-btn">
            <div>Services</div>
          </div>
        </a>
      </nav>
      <div class="wrapper">
        <div class="nav-link">
          <div class="div-block"><img src="images/Tilda_Icons_-10.svg" alt="" class="icon-btn">
            <div class="html-embed-2 w-embed"><button style="background-color: rgba(25, 32, 36, 0.0);" onclick="$crisp.push(['do', 'chat:open'])">Chat Support</button></div>
          </div>
        </div>
        <div class="wrapper-flex-horizontal in-nav">
          <a href="#" class="nav-link not-active w-inline-block">
            <div class="div-block"><img src="images/person.svg" alt="" class="icon-btn">
              <div>Log In</div>
            </div>
          </a>
          <a href="#" class="nav-link flex w-inline-block">
            <div class="div-block"><img src="images/settings.svg" alt="" class="icon-btn no-margin-right"></div>
          </a>
        </div>
        <div class="wrapper-flex-horizontal in-nav">
          <a href="#" class="nav-link not-active w-inline-block">
            <div class="div-block"><img src="images/person.svg" alt="" class="icon-btn">
              <div>Log In</div>
            </div>
          </a>
          <a href="#" class="nav-link flex not-active w-inline-block">
            <div class="div-block"><img src="images/settings.svg" alt="" class="icon-btn no-margin-right"></div>
          </a>
        </div>
      </div>
      <div class="menu-button w-nav-button">
        <div class="icon w-icon-nav-menu"></div>
      </div>
    </div>
  </div>