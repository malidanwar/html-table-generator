<!DOCTYPE html>
<!--  This site was created in Webflow. http://www.webflow.com  -->
<!--  Last Published: Mon Nov 11 2019 17:29:53 GMT+0000 (UTC)  -->
<html data-wf-page="5dc93d0cc19d438c5e0b8bab" data-wf-site="5ca998831283e552de07a9c9">
<head>
  <meta charset="utf-8">
  <title>Service profile -&quot;no integrations&quot; version</title>
  <meta content="Service profile -&quot;no integrations&quot; version" property="og:title">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="css/normalize.css" rel="stylesheet" type="text/css">
  <link href="css/webflow.css" rel="stylesheet" type="text/css">
  <link href="css/ensemblemind.webflow.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" type="text/javascript"></script>
  <script type="text/javascript">WebFont.load({  google: {    families: ["Merriweather:300,300italic,400,400italic,700,700italic,900,900italic","Roboto:300,regular,500,700,900"]  }});</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
  <link href="images/favicon.jpg" rel="shortcut icon" type="image/x-icon">
  <link href="images/webclip.jpg" rel="apple-touch-icon">
  <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="454376d4-f054-46ee-9f05-a8fe5305d98b";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
</head>
<body class="body-service">
@include('sidebar')
  <div id="service" class="section">
    <div class="container relative">
      <div class="white-box fixed-size">
        <div class="title-wrapper">
          <div class="container-for-title cta mob">
            <div class="cta-wrapper"><img src="images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f.png" srcset="images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f-p-500.png 500w, images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f.png 512w" sizes="(max-width: 991px) 100px, 100vw" alt="" class="service-ico">
              <h2>Dendritic spine analysis</h2>
            </div>
            <div class="container-for-title company-in-cta"><img src="images/favicon_256.png" alt="" class="company-image small">
              <div class="text-city">Afraxis, Inc., San Diego, USA</div>
            </div>
          </div>
          <div class="container-for-title desk"><img src="images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f.png" srcset="images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f-p-500.png 500w, images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f.png 512w" sizes="(max-width: 991px) 100vw, 100px" alt="" class="service-ico">
            <h2>Dendritic spine analysis</h2>
          </div>
          <div class="wrapper-flex-horizontal"><a id="w-node-c9be28851389-5e0b8bab" data-w-id="edf1a5fa-26c8-2469-436f-c9be28851389" href="#" class="button additional-explore w-inline-block"><img src="images/Tilda_Icons_30_system_play.svg" style="display:block" alt="" class="icon-btn"><img src="images/add.svg" style="display:none" alt="" class="icon-btn-white"><div>Explore</div></a>
            <div id="w-node-c9be2885138e-5e0b8bab" class="button proposal"><img src="images/Tilda_Icons_37_Finance-technologies_big-data.svg" alt="" class="icon-btn">
              <div class="html-embed w-embed w-script"><a class="typeform-share link" href="https://afraxis.typeform.com/to/nOX8m3" data-mode="popup" style="color:#ffffff;text-decoration:none;font-size:2vh;" target="_blank">Request a Quote</a>
                <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
              </div>
            </div>
          </div>
        </div>
        <h3>About Service</h3>
        <div class="divider"></div>
        <div class="paragraph-scroll">
          <p class="pragraph-justify">Afraxis Inc. is a contract research organization (CRO) specializing in:</p>
          <ol class="list-bullets">
            <li>
              <p>CNS preclinical efficacy evaluations (in vivo &amp; in vitro);</p>
            </li>
            <li>
              <p>Neurotoxicity evaluations;</p>
            </li>
            <li>
              <p>Structure-Efficacy Relationship (SER) medicinal chemistry;</p>
            </li>
            <li>
              <p>Novel powerful statistical methods tailored for big data analyses;</p>
            </li>
          </ol>
          <p class="pragraph-justify">The process incorporates (a) specialized labeling of individual neurons, (b) laser-scanning microscopy, (c) rapid and high-detail morphometry of individual synapses, and (d) novel powerful statistical methods tailored for big data analyses. The Afraxis ESP platform was first developed within the original Afraxis, Inc. (now Afraxis Holdings), a company founded in 2007 to develop novel therapeutics for Fragile-X Syndrome (FXS) based on the work of Nobel laureate Susumu Tonegawa. The ESP technology was developed originally to solve an unmet need in Fragile-X drug discovery: the lack of a rapid tool to evaluate test compounds against dendritic spine abnormalities, the hallmark neuorbiological phenotype of FXS observed in both humans and rodent models. Considering the broad reach of dendritic spine abnormalitiies throughout CNS diseases, the tool was immediately applicable for other indications. The current Afraxis, Inc. was founded in April 2013 as a spin-out company by Christopher Rex and Carmine Stengone to develop the core ESP technology and drug discovery services.</p>
        </div>
      </div>
      <div class="white-box wrapper"></div>
      <div data-w-id="126b3d30-8ff4-572c-273a-d7293d96e42a" style="-webkit-transform:translate3d(0, 300PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0, 300PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0, 300PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0, 300PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)" class="white-box fixed-size cta-pop-up">
        <div class="title-wrapper cta">
          <div class="container-for-title cta">
            <div class="cta-wrapper"><img src="images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f.png" srcset="images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f-p-500.png 500w, images/018-brain-f2aa415b7f5dfc7b1d2b2281007531d0f5f6f9b55fc94e6593bf15ef00ef7c8f.png 512w" sizes="100px" alt="" class="service-ico">
              <h2>Dendritic spine analysis</h2>
            </div>
          </div>
          <div class="button proposal cta"><img src="images/Tilda_Icons_37_Finance-technologies_big-data.svg" alt="" class="icon-btn">
            <div class="html-embed w-embed w-script"><a class="typeform-share link" href="https://afraxis.typeform.com/to/nOX8m3" data-mode="popup" style="color:#ffffff;text-decoration:none;font-size:2vh;" target="_blank">Request a Quote</a>
              <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script>
            </div>
          </div>
        </div>
      </div>
      <div class="container-inside second">
        <div class="white-box fixed-small first no-margin">
          <div class="subtitle-wrapper full-width">
            <h3>Related documents</h3>
            <div class="divider"></div>
          </div>
          <div class="wrapper-flex-horizontal">
            <div class="wrapper-vertical margin-right">
              <a href="documents/Afraxis-ESP-Overview---Jan-2019.pdf" target="_blank" class="service-link w-inline-block">
                <div class="container-small">
                  <div class="text-ico-wrapper"><img src="images/Tilda_Icons_31_format_pdf.svg" alt="" class="service-ico big">
                    <p class="paragraph-s">Overview of dendritic spine analysis/copy</p>
                  </div>
                </div>
              </a>
              <a href="documents/Afraxis-ESP-Overview---Jan-2019.pdf" target="_blank" class="service-link w-inline-block">
                <div class="container-small">
                  <div class="text-ico-wrapper"><img src="images/Tilda_Icons_31_format_pdf.svg" alt="" class="service-ico big">
                    <p class="paragraph-s">Overview of dendritic spine analysis/copy</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="wrapper-vertical">
              <a href="documents/Afraxis-ESP-Overview---Jan-2019.pdf" target="_blank" class="service-link w-inline-block">
                <div class="container-small">
                  <div class="text-ico-wrapper"><img src="images/Tilda_Icons_31_format_pdf.svg" alt="" class="service-ico big">
                    <p class="paragraph-s">Overview of dendritic spine analysis/copy</p>
                  </div>
                </div>
              </a>
              <a href="documents/Afraxis-ESP-Overview---Jan-2019.pdf" target="_blank" class="service-link w-inline-block">
                <div class="container-small">
                  <div class="text-ico-wrapper"><img src="images/Tilda_Icons_31_format_pdf.svg" alt="" class="service-ico big">
                    <p class="paragraph-s">Overview of dendritic spine analysis/copy</p>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="container relative-small mob">
        <div class="white-box fixed-small">
          <div class="w-layout-grid grid-title"><a id="w-node-b506d62d0f3f-5e0b8bab" href="https://afraxis.com/" target="_blank" class="company-link w-inline-block"><img src="images/favicon_256.png" alt="" class="company-image"></a>
            <div id="w-node-b506d62d0f41-5e0b8bab" class="subtitle-wrapper">
              <h3>Afraxis, Inc.</h3>
              <div class="text-city">San Diego, USA</div>
            </div>
          </div>
          <div class="divider"></div>
          <p class="margin-bot">Superresolution microscopy combined with AI-powered image analysis</p>
          <a href="#" class="lightbox-link w-inline-block w-lightbox">
            <p class="paragraph-center">In 30 seconds about company</p><img src="images/Tilda_Icons_-4.svg" width="40" alt="" class="grid-image-video">
            <script type="application/json" class="w-json">{
  "items": [
    {
      "type": "video",
      "originalUrl": "https://www.youtube.com/watch?v=o7gGqAQWHg0",
      "url": "https://www.youtube.com/watch?v=o7gGqAQWHg0",
      "html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Fo7gGqAQWHg0%3Ffeature%3Doembed&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Do7gGqAQWHg0&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Fo7gGqAQWHg0%2Fhqdefault.jpg&key=96f1f04c5f4143bcb0f2e68c87d65feb&type=text%2Fhtml&schema=youtube\" width=\"940\" height=\"528\" scrolling=\"no\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen=\"true\"></iframe>",
      "thumbnailUrl": "https://i.ytimg.com/vi/o7gGqAQWHg0/hqdefault.jpg",
      "width": 940,
      "height": 528
    }
  ]
}</script>
          </a>
        </div>
        <div class="white-box fixed-small second">
          <div class="subtitle-wrapper full-width">
            <h3>More services by Afraxis, Inc.</h3>
            <div class="divider"></div>
          </div>
          <a href="#" class="service-link w-inline-block">
            <div class="container-small">
              <div class="text-ico-wrapper"><img src="images/006-dna.svg" id="w-node-b506d62d0f55-5e0b8bab" alt="" class="service-ico big">
                <p id="w-node-b506d62d0f56-5e0b8bab" class="paragraph-s">Service Title #1. Second line for long title</p>
              </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
          </a>
          <a href="#" class="service-link w-inline-block">
            <div class="container-small">
              <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
                <p class="paragraph-s">Service Title #2 short title</p>
              </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
          </a>
          <a href="#" class="service-link w-inline-block">
            <div class="container-small">
              <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
                <p class="paragraph-s">Service Title#3. Second line for long title</p>
              </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
          </a>
          <a href="#" class="service-link w-inline-block">
            <div class="container-small">
              <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
                <p class="paragraph-s">Service Title #4 second line</p>
              </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
          </a>
          <a href="#" class="service-link w-inline-block">
            <div class="container-small">
              <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
                <p class="paragraph-s">Service Title #5. Second line for long title</p>
              </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
          </a>
        </div>
      </div>
    </div>
    <div class="container relative-small desktop">
      <div class="white-box fixed-small">
        <div class="w-layout-grid grid-title">
          <a href="https://afraxis.com/" target="_blank" class="company-link w-inline-block">
            <div class="company-image"></div>
          </a>
          <div>
            <div class="subtitle-wrapper">
              <h3>Afraxis, Inc.</h3>
              <div class="text-city">San Diego, USA</div>
            </div>
          </div>
        </div>
        <div class="divider"></div>
        <p class="margin-bot">Superresolution microscopy combined with AI-powered image analysis</p>
        <div class="video-back">
          <a href="#" class="lightbox-link w-inline-block w-lightbox">
            <p class="paragraph-center white">In 30 seconds about company</p><img src="images/social-16-white.svg" width="40" alt="" class="grid-image-video">
            <script type="application/json" class="w-json">{
  "items": [
    {
      "type": "video",
      "originalUrl": "https://www.youtube.com/watch?v=o7gGqAQWHg0",
      "url": "https://www.youtube.com/watch?v=o7gGqAQWHg0",
      "html": "<iframe class=\"embedly-embed\" src=\"//cdn.embedly.com/widgets/media.html?src=https%3A%2F%2Fwww.youtube.com%2Fembed%2Fo7gGqAQWHg0%3Ffeature%3Doembed&url=http%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3Do7gGqAQWHg0&image=https%3A%2F%2Fi.ytimg.com%2Fvi%2Fo7gGqAQWHg0%2Fhqdefault.jpg&key=96f1f04c5f4143bcb0f2e68c87d65feb&type=text%2Fhtml&schema=youtube\" width=\"940\" height=\"528\" scrolling=\"no\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen=\"true\"></iframe>",
      "thumbnailUrl": "https://i.ytimg.com/vi/o7gGqAQWHg0/hqdefault.jpg",
      "width": 940,
      "height": 528
    }
  ]
}</script>
          </a>
        </div>
      </div>
      <div class="white-box fixed-small second">
        <div class="subtitle-wrapper full-width">
          <h3>More services by Afraxis, Inc.</h3>
          <div class="divider"></div>
        </div>
        <a href="#" class="service-link w-inline-block">
          <div class="container-small">
            <div class="text-ico-wrapper"><img src="images/006-dna.svg" id="w-node-69c036e95feb-5e0b8bab" alt="" class="service-ico big">
              <p id="w-node-69c036e95fec-5e0b8bab" class="paragraph-s">Service Title #1. Second line for long title</p>
            </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
        </a>
        <a href="#" class="service-link w-inline-block">
          <div class="container-small">
            <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
              <p class="paragraph-s">Service Title #2 short title</p>
            </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
        </a>
        <a href="#" class="service-link w-inline-block">
          <div class="container-small">
            <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
              <p class="paragraph-s">Service Title#3. Second line for long title</p>
            </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
        </a>
        <a href="#" class="service-link w-inline-block">
          <div class="container-small">
            <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
              <p class="paragraph-s">Service Title #4 second line</p>
            </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
        </a>
        <a href="#" class="service-link w-inline-block">
          <div class="container-small">
            <div class="text-ico-wrapper"><img src="images/006-dna.svg" alt="" class="service-ico big">
              <p class="paragraph-s">Service Title #5. Second line for long title</p>
            </div><img src="images/menu-icon.svg" alt="" class="service-ico menu"></div>
        </a>
      </div>
    </div>
  </div>
  <div data-collapse="medium" data-animation="default" data-duration="400" class="navigation-bar w-nav">
    <div class="nav-container w-container"><a href="dendritic-spine-analysis.html" class="brand-link w-nav-brand"><img src="images/logofull_EM.png" width="173" srcset="images/logofull_EM-p-500.png 500w, images/logofull_EM-p-800.png 800w, images/logofull_EM-p-1080.png 1080w, images/logofull_EM-p-1600.png 1600w, images/logofull_EM-p-2000.png 2000w, images/logofull_EM.png 2422w" sizes="100vw" alt=""></a>
      <div class="nav-descript">Created by scientists <br>for scientists</div>
      <nav role="navigation" id="w-node-bcc753e70c34-cf81c401" class="navigation-menu w-nav-menu"><a href="dendritic-spine-analysis.html" class="navigation-link w-nav-link">Home</a><a href="dendritic-spine-analysis.html" class="navigation-link w-nav-link">Product</a><a href="dendritic-spine-analysis.html" class="navigation-link w-nav-link">Solutions</a><a href="contact.html" class="navigation-link w-nav-link">Contact</a><a href="contact.html" class="navigation-link w-nav-link">Log In</a></nav>
      <div id="w-node-f786d2c31d29-cf81c401" class="hamburger-button w-nav-button">
        <div class="w-icon-nav-menu"></div>
      </div>
    </div>
  </div>
  <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.4.1.min.220afd743d.js" type="text/javascript" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="js/webflow.js" type="text/javascript"></script>
  <!-- [if lte IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script><![endif] -->
</body>
</html>